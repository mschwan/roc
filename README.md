Reflow oven control firmware
===

This is a work-in-progress software for a reflow oven controller.

Requirements
---

The projects depends on AVR.

Application and usage
---

As the things you find here are still from the dragon lands a lot is changing.
Use with caution!

Building and running
---

To build and run this software connect your AVR In-System Programmer (e.g. an
Arduino Uno with the upload ISP project) via USB to your computer. Open a
terminal, change to the directory of this project and execute the following
commands:
```sh
$ make
```

Documentation
---

Build the documentation with
```sh
$ make doc
```
and open `doc/html/index.html` afterwards in your preferred HTML browser.

License
---

This project is licensed under the [GNU
GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html). See the LICENSE file for
detailed information.
