MCU = atmega328p
CLOCK = 20000000
PROG = arduino
PORT = /dev/ttyACM0
BAUD = 19200

NAME = roc
SRCDIR = src
BINDIR = bin
OBJ = $(BINDIR)/main.o \
	  $(BINDIR)/dogm163.o \
	  $(BINDIR)/max31855.o
CC = avr-gcc
CFLAGS = -std=c99 -Wl,-u,vfprintf -lprintf_flt -lm -Os \
		 -Wall -Wstrict-prototypes -pedantic \
		 -DF_CPU=$(CLOCK) -mmcu=$(MCU)
LFUSE = 0xE6

.PHONY: doc tags clean cleanall

all: build upload

$(BINDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c $^ -o $@

build: $(OBJ)
	$(CC) $(CFLAGS) $^ -o $(BINDIR)/$(NAME).elf
	avr-objcopy -j .text -j .data -O ihex \
		$(BINDIR)/$(NAME).elf $(BINDIR)/$(NAME).hex

upload:
	avrdude -v -p $(MCU) -c $(PROG) -P $(PORT) -b $(BAUD) \
		-U flash:w:$(BINDIR)/$(NAME).hex:i

fuses:
	avrdude -v -p $(MCU) -c $(PROG) -P $(PORT) -b $(BAUD) \
		-U lfuse:w:$(LFUSE):m

doc:
	doxygen doxyfile

tags:
	ctags -R -f tags src/ /usr/lib/avr/include/

clean:
	rm -f $(OBJ)
	rm -f $(BINDIR)/$(NAME).elf

cleanall: clean
	rm -f tags
	rm -f $(BINDIR)/$(NAME).hex
