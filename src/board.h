/*
    Reflow oven control firmware
    Copyright (C) 2017  Martin Schwan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BOARD_H
#define BOARD_H

#include <stdbool.h>

/** Delay inside the main loop in milliseconds*/
#define LOOP_DELAY  200

/** Time in seconds, only increases when the hysteresis controller started */
extern uint16_t time_g;
/** Boolean value holding the "running" state of the hysteresis controller */
extern bool is_running_g;

#endif // BOARD_H
