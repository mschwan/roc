/*
    Reflow oven control firmware
    Copyright (C) 2017  Martin Schwan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "max31855.h"

static struct max31855 *device_m;
static bool is_init_m = false;
static void delay_m(void);
static uint32_t spi_read_m(void);

void max31855_init(struct max31855 *device)
{
    if(is_init_m) {
        return;
    }

    device_m = device;

    // CS output
    *device_m->ddr |= (1 << device_m->cs);
    // CS high
    *device_m->port |= (1 << device_m->cs);
    // CLK output
    *device_m->ddr |= (1 << device_m->clk);
    // CLK high
    *device_m->port |= (1 << device_m->clk);
    // MISO input
    *device_m->ddr &= ~(1 << device_m->miso);

    is_init_m = true;
}

float max31855_read_external(void)
{
    uint32_t val;
    float res;

    val = spi_read_m();

    if(val & 0x07) {
        return -999.0;
    }

    if(val & 0x80000000) {
        // negative value
        val = 0xFFFFC000 | ((val >> 18) & 0x0003FFFF);
    } else {
        // positive value
        val >>= 18;
    }

    res = val;
    res *= 0.25;

    return res;
}

void delay_m(void)
{
    switch(device_m->speed) {
        case MAX31855_SLOW:
            _delay_ms(1);
            break;
        case MAX31855_MEDIUM:
            _delay_us(100);
            break;
        case MAX31855_FAST:
            _delay_us(10);
            break;
        case MAX31855_FASTEST:
            // the only delay here is caused by calling this function, so it
            // heavily depends on your microcontroller's operating frequency
            // what the actual bitrate will be
            break;
        default:
            _delay_us(10);
            break;
    }
}

uint32_t spi_read_m(void)
{
    if(!is_init_m) {
        return 0xFFFFFFFF;
    }

    uint32_t data = 0;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
        *device_m->port &= ~(1 << device_m->cs);
        delay_m();

        for(int8_t i = 31; i >= 0; i--) {
            *device_m->port &= ~(1 << device_m->clk);
            delay_m();
            data <<= 1;

            if(*device_m->pin & (1 << device_m->miso)) {
                data |= 1;
            }

            *device_m->port |= (1 << device_m->clk);
            delay_m();
        }

        *device_m->port |= (1 << device_m->cs);
    }

    return data;
}
