/*
    Reflow oven control firmware
    Copyright (C) 2017  Martin Schwan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DOGM163_H
#define DOGM163_H

#include <avr/io.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <util/delay.h>

/** The base address of the character data in the RAM. */
#define DOGM163_BASE_ADDR  0x80
/** The display's height in number of characters */
#define DOGM163_HEIGHT     3
/** The display's width in number of characters */
#define DOGM163_WIDTH      16

/** Data structure holding the pin information of a connected DOGM163 display.
 *
 * @note  All pins must be connected to the same port as the SPI. E.g. using CS
 * on a pin of port C while SPI is on port B is not supported yet!
 */
struct dogm163_dev {
    /** Data direction register */
    volatile uint8_t *ddr;
    /** Port register */
    volatile uint8_t *port;
    /** MISO pin number */
    uint8_t pin_mosi;
    /** CLK pin number */
    uint8_t pin_clk;
    /** CS pin number */
    uint8_t pin_cs;
    /** RS pin number, used to decide between writing an instruction or data
     * write operation
     */
    uint8_t pin_rs;
};

/** Initialize a DOGM163 display.
 *
 * @param dev  the device data structure to initialize.
 * @return  0 if successful, -1 if a device has already been initialized.
 */
int dogm163_init(struct dogm163_dev *dev);

/** Release an initialized DOGM163 display.
 * This releases the data structure previously bound with dogm163_init().
 */
void dogm163_release(void);

/** Set the cursor position on the display.
 * The bounds are given by DOGM163_WIDTH and DOGM163_HEIGHT.
 *
 * @param row  the row on the display in number of characters.
 * @param col  the column on the display in number of characters.
 * @return  0 if successful, -1 if one of row or col are out of the display's
 *          bounds.
 */
int dogm163_set_cursor(uint8_t row, uint8_t col);

/** Write a string to the display at the current cursor position.
 *
 * @param str  the string to display.
 */
void dogm163_write(char *str);

#endif // DOGM163_H
