/*
    Reflow oven control firmware
    Copyright (C) 2017  Martin Schwan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAX31855_H
#define MAX31855_H

#include <avr/io.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <util/atomic.h>
#include <util/delay.h>

/** Transfer speed of the software SPI connection. */
enum max31855_transfer_speed {
    /** Slow transfer speed */
    MAX31855_SLOW,
    /** Medium transfer speed */
    MAX31855_MEDIUM,
    /** Fast transfer speed */
    MAX31855_FAST,
    /** Fastest possible transfer speed */
    MAX31855_FASTEST
};

/** Data structure of a connected MAX31855 device. */
struct max31855 {
    /** Data direction register */
    volatile uint8_t *ddr;
    /** Port register */
    volatile uint8_t *port;
    /** Port input register */
    volatile uint8_t *pin;
    /** CS pin number */
    uint8_t cs;
    /** CLK pin number */
    uint8_t clk;
    /** MISO pin number */
    uint8_t miso;
    /** Transfer speed */
    enum max31855_transfer_speed speed;
};

/** Initialize a MAX31855 and its connected pins.
 *
 * @param device  is the MAX31855 with its connected pins to initialize.
 */
void max31855_init(struct max31855 *device);

/** Read the temperature value of the (external) thermocouple.
 *
 * @return  the temperature measured by the thermocouple in degree Celcius.
 */
float max31855_read_external(void);

#endif // MAX31855_H
