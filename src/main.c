/*
    Reflow oven control firmware
    Copyright (C) 2017  Martin Schwan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "board.h"
#include "dogm163.h"
#include "max31855.h"

uint16_t time_g = 0;
bool is_running_g = false;

ISR(TIMER1_COMPA_vect)
{
    if(is_running_g) {
        time_g++;
    }

    PORTD ^= (1 << PD3);
}

int main(void)
{
    // DOGM163 display
    struct dogm163_dev display = {
        .ddr = &DDRB,
        .port = &PORTB,
        .pin_mosi = 3,
        .pin_clk = 5,
        .pin_cs = 2,
        .pin_rs = 1
    };
    dogm163_init(&display);
    char string[DOGM163_WIDTH + 1];

    // 1s timer
    TCCR1A = 0;  // normal port operation, OC1A/OC1B disconnected
    TCCR1B |= (1 << CS12) | (1 << CS10);  // prescale by 1024
    TCCR1B |= (1 << WGM12);  // top is OCR1A
    TCNT1 = 0;  // reset counter
    OCR1A = 19531;  // 20MHz / 1024 / 19531 = 1/s
    TIMSK1 |= (1 << OCIE1A);  // enable timer/counter 1A

    // MAX31855 temperature sensor
    struct max31855 temp_sensor = {
        .ddr = &DDRD,
        .port = &PORTD,
        .pin = &PIND,
        .cs = 7,
        .clk = 6,
        .miso = 5,
        .speed = MAX31855_FAST
    };
    max31855_init(&temp_sensor);
    float temp;

    // start button
    DDRD &= ~(1 << DDD2);

    // clock LED
    DDRD |= (1 << DDD3);
    PORTD &= ~(1 << PD3);

    // SSR
    DDRB |= (1 << DDB0);
    PORTB &= ~(1 << PB0);

    // hysteresis controller
    float setpoint = 0;
    // counter for the solderpaste temperature array
    uint8_t k = 0;
    // solderpaste temperature curve, interpolate linearly between setpoints
    int16_t test_curve[10][4] = {
        {0, 25},
        {50, 40},
        {140, 150},
        {200, 170},
        {260, 220},
        {290, 240},
        {320, 215},
        {350, 150},
        {400, 100},
        {500, 50}
    };

    sei();

    while(1) {
        _delay_ms(LOOP_DELAY);

        if(time_g > 500) {
            is_running_g = false;
            time_g = 0;
            k = 0;
            setpoint = 0;
        }

        sprintf(string, "Time: %3ds", time_g);
        dogm163_set_cursor(0, 0);
        dogm163_write(string);

        // check if controller should start
        if(!is_running_g && (PIND & (1 << PIND2))) {
            is_running_g = true;
        }

        // MAX31855 reading
        temp = max31855_read_external();
        sprintf(string, "Temp.: %7.2f\xDF\x43", temp);
        dogm163_set_cursor(2, 0);
        dogm163_write(string);

        // mbb controller
        setpoint = (float) (test_curve[k+1][1] - test_curve[k][1])
            / (float) (test_curve[k+1][0] - test_curve[k][0])
            * (float) (time_g - test_curve[k][0]) + (float) test_curve[k][1];

        sprintf(string, "Setp.: %7.2f\xDF\x43", setpoint);
        dogm163_set_cursor(1, 0);
        dogm163_write(string);

        if(time_g >= test_curve[k+1][0]) {
            k++;
            setpoint = (float) test_curve[k][1];
        }

        if(is_running_g && temp <= setpoint) {
            PORTB |= (1 << PB0);
        } else {
            PORTB &= ~(1 << PB0);
        }
    }

    cli();

    return 0;
}
