/*
    Reflow oven control firmware
    Copyright (C) 2017  Martin Schwan

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "dogm163.h"

static struct dogm163_dev *dev_m;
static bool is_init_m = false;
static void spi_init_m(void);
static void spi_write_m(char data);
static void write_instr_m(char instr);
static void write_data_m(char data);
static void init_m(void);  // TODO: pass init options

// global

int dogm163_init(struct dogm163_dev *dev)
{
    if(is_init_m || dev == NULL) {
        return -1;
    }

    dev_m = dev;

    // wait for supply voltage to stabilize
    _delay_ms(50);
    spi_init_m();
    init_m();
    is_init_m = true;

    return 0;
}

void dogm163_release(void)
{
    dev_m = NULL;
    is_init_m = false;
}

int dogm163_set_cursor(uint8_t row, uint8_t col)
{
    if(row >= DOGM163_HEIGHT || col >= DOGM163_WIDTH) {
        return -1;
    }

    write_instr_m(DOGM163_BASE_ADDR + row * DOGM163_WIDTH + col);
    return 0;
}

void dogm163_write(char *str)
{
    while(*str) {  // while *str not null, thus not at end
        write_data_m(*str);  // write first char element of string
        str++;  // increment index of first char element in string
    }
}

// local

void spi_init_m(void)
{
    // data direction
    *dev_m->ddr |= (1 << dev_m->pin_mosi) | (1 << dev_m->pin_clk)
        | (1 << dev_m->pin_cs) | (1 << dev_m->pin_rs);
    // deselect chip
    *dev_m->port |= (1 << dev_m->pin_cs);

    // enable hardware SPI
    SPCR |= (1 << SPE) | (1 << MSTR) | (1 << SPR0);
}

void spi_write_m(char data)
{
    *dev_m->port &= ~(1 << dev_m->pin_cs);
    SPDR = data;
    while(!(SPSR & (1 << SPIF)));
    *dev_m->port |= (1 << dev_m->pin_cs);
}

void write_instr_m(char instr)
{
    *dev_m->port &= ~(1 << dev_m->pin_rs);
    spi_write_m(instr);
    _delay_ms(1);
}

void write_data_m(char data)
{
    *dev_m->port |= (1 << dev_m->pin_rs);
    spi_write_m(data);
    _delay_ms(1);
}

void init_m(void)
{
    write_instr_m(0x38);  // instruction set
    write_instr_m(0x39);  // instruction set
    write_instr_m(0x15);  // bias
    write_instr_m(0x7F);  // contrast
    write_instr_m(0x5F);  // power, icon, contrast
    write_instr_m(0x69);  // follower
    write_instr_m(0x0C);  // display function
    write_instr_m(0x01);  // clear display
    write_instr_m(0x06);  // entry mode
}
